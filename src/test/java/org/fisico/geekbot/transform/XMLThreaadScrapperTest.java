package org.fisico.geekbot.transform;

import org.fisico.geekbot.model.BGGPost;
import org.fisico.geekbot.model.BGGSideError;
import org.fisico.geekbot.model.BGGThread;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;


public class XMLThreaadScrapperTest {

    @Test
    public void testArticleCorrrect() {

        XMLThreadScrapper stu = new XMLThreadScrapper();

        String xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?><thread id=\"1408773\" numarticles=\"4\" link=\"http://boardgamegeek.com/thread/1408773\" termsofuse=\"http://boardgamegeek.com/xmlapi/termsofuse\">\n" +
                "<subject>Virtual Croupier Test</subject>\n" +
                "<articles>\n" +
                "\t<article id=\"19901306\" username=\"virtualcroupier\" link=\"http://boardgamegeek.com/article/19901306#19901306\" postdate=\"2015-07-28T12:54:07-05:00\" editdate=\"2015-07-28T12:54:07-05:00\" numedits=\"0\">\n" +
                "\t\t<subject>Virtual Croupier Test</subject>\n" +
                "\t\t<body>Just a thread to test Virtual Croupier. Nothing to see... yet</body>\n" +
                "\t</article>\n" +
                "\t<article id=\"19901403\" username=\"virtualcroupier\" link=\"http://boardgamegeek.com/article/19901403#19901403\" postdate=\"2015-07-28T13:02:45-05:00\" editdate=\"2015-07-28T13:02:45-05:00\" numedits=\"0\">\n" +
                "\t\t<subject>Re: Virtual Croupier Test</subject>\n" +
                "\t\t<body>Test 1</body>\n" +
                "\t</article>\n" +
                "\t<article id=\"20118211\" username=\"virtualcroupier\" link=\"http://boardgamegeek.com/article/20118211#20118211\" postdate=\"2015-08-21T10:25:17-05:00\" editdate=\"2015-08-21T10:25:17-05:00\" numedits=\"0\">\n" +
                "\t\t<subject>Re: Virtual Croupier Test</subject>\n" +
                "\t\t<body>Test 3</body>\n" +
                "\t</article>\n" +
                "\t<article id=\"20118337\" username=\"virtualcroupier\" link=\"http://boardgamegeek.com/article/20118337#20118337\" postdate=\"2015-08-21T10:36:17-05:00\" editdate=\"2015-08-21T10:36:17-05:00\" numedits=\"0\">\n" +
                "\t\t<subject>Subject changed</subject>\n" +
                "\t\t<body>Test 2</body>\n" +
                "\t</article>\n" +
                "</articles>\n" +
                "</thread>";

        try {
            BGGThread thread = stu.parse(xml);
            Assert.assertEquals(1408773, thread.getId());
            List<BGGPost> articles = thread.getArticles();
            Assert.assertEquals(4, articles.size());
            BGGPost article = articles.get(1);
            Assert.assertEquals(19901403, article.getId());
            Assert.assertEquals("Test 1", article.getBody());
            Assert.assertEquals("virtualcroupier", article.getUser());
        } catch (BGGSideError bggSideError) {
            Assert.fail();
        }

    }
}
