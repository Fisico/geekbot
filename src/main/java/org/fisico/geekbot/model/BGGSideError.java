package org.fisico.geekbot.model;

public class BGGSideError extends Exception {


    private static final long serialVersionUID = -4920971140977612710L;

    public BGGSideError(Exception e) {
        super(e);
    }

    public BGGSideError(String message) {
        super(message);
    }
}
