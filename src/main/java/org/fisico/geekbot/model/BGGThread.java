package org.fisico.geekbot.model;

import java.util.List;

public class BGGThread {
    private int id;
    private String subject;
    private List<BGGPost> articles;

    public BGGThread(int id, String subject) {
        super();
        this.id = id;
        this.subject = subject;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public List<BGGPost> getArticles() {
        return articles;
    }

    public void setArticles(List<BGGPost> articles) {
        this.articles = articles;
    }
}
