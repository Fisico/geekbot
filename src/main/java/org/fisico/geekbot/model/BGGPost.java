package org.fisico.geekbot.model;

public class BGGPost {
    private int id;
    private String user;
    private String subject;
    private String body;

    public BGGPost(int id, String user, String subject, String body) {
        super();
        this.id = id;
        this.user = user;
        this.subject = subject;
        this.body = body;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
