package org.fisico.geekbot.model;

public class GeekBotException extends Exception {

    public GeekBotException(String message, Exception cause) {
        super(message, cause);
    }
}
