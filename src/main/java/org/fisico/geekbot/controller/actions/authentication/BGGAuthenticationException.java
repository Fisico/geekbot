package org.fisico.geekbot.controller.actions.authentication;

public class BGGAuthenticationException extends Exception {

    public BGGAuthenticationException() {
        super();
    }

    public BGGAuthenticationException(String message, Throwable cause) {
        super(message, cause);
    }

    public BGGAuthenticationException(Throwable cause) {
        super(cause);
    }

    protected BGGAuthenticationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
