package org.fisico.geekbot.controller.actions;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.fisico.geekbot.controller.actions.authentication.LoginInfo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;

public abstract class BggHttpAction {

    public HttpResponse execute(LoginInfo login, HttpEntity entity, HttpClientContext context) throws IOException {
        final HttpResponse resp;
        try {
            URI bggUri = getUri();
            if (login != null && login.isValid()) {
                context.setCookieStore(login.getStore());
            }
            HttpRequestBase request = getRequest(entity, bggUri);
            CloseableHttpClient httpClient = HttpClients.createDefault();
            resp = httpClient.execute(request, context);
        } catch (ClientProtocolException e) {
            throw new IOException(e);
        }

        return resp;
    }

    protected abstract HttpRequestBase getRequest(HttpEntity entity, URI bggUri);

    protected abstract URI getUri();

    protected String readHTMLPage(InputStream content) throws IOException {
        String inputLine;
        BufferedReader in;
        StringBuilder sb = new StringBuilder();
        try {
            in = new BufferedReader(new InputStreamReader(content));
            while ((inputLine = in.readLine()) != null) {
                sb.append(inputLine);
            }
        } finally {
            try {
                if (content != null) {
                    content.close();
                }
            } catch (IOException e) {
            }
        }

        return sb.toString();
    }
}
