package org.fisico.geekbot.controller.actions.authentication;


import org.fisico.geekbot.model.BGGSideError;

import java.util.HashMap;
import java.util.Map;

public class AuthenticationService {

    private final String PROTOCOL = "https";
    private final String HOST = "www.boardgamegeek.com";
    private final String ACTION = "/login";
    private final String INVALID_USERNAME_PASSWORD = "Invalid Username/Password";

    private final Map<Credentials, LoginInfo> credentialsKnown;
    private AuthenticationAction authenticationAction;

    public AuthenticationService() {
        credentialsKnown = new HashMap<>();
    }

    public LoginInfo authenticate(Credentials credentials) throws BGGAuthenticationException, BGGSideError {
        if (credentialsKnown.containsKey(credentials)) {
            LoginInfo login = credentialsKnown.get(credentials);
            if (login.isValid()) {
                return login;
            }
        }
        LoginInfo login = authenticationAction.authenticate(credentials);
        credentialsKnown.put(credentials, login);

        return login;
    }
}
