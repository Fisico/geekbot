package org.fisico.geekbot.controller.actions.thread;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIBuilder;
import org.fisico.geekbot.controller.actions.BGGHttpGetAction;
import org.fisico.geekbot.model.GeekBotException;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;

@Component
public class ThreadFinder extends BGGHttpGetAction {
    private final String PROTOCOL = "https";
    private final String HOST = "www.boardgamegeek.com";
    private final String ACTION = "/article/";
    private int articleId;

    public int findParentThreadId(int articleId) throws GeekBotException {
        try {
            this.articleId = articleId;
            HttpClientContext context = HttpClientContext.create();
            HttpResponse resp = execute(null, null, context);
            return resolveResult(context, resp);
        } catch (Exception e) {
            throw new GeekBotException("Error finding the id of the thread corresponding to the article " + articleId, e);
        }
    }

    @Override
    protected URI getUri() {
        URI bggUri = null;
        try {
            bggUri = new URIBuilder()
                    .setScheme(PROTOCOL)
                    .setHost(HOST)
                    .setPath(ACTION + articleId)
                    .build();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return bggUri;
    }

    private int resolveResult(HttpClientContext context, HttpResponse resp) throws IOException {
        int parentId = -1;
        int responseCode = resp.getStatusLine().getStatusCode();
        if (responseCode == HttpStatus.SC_OK) {
            try (BufferedReader br = new BufferedReader(new InputStreamReader(resp.getEntity().getContent()))) {
                String inputLine;
                while ((inputLine = br.readLine()) != null) {
                    if (inputLine.contains("<link rel=\"canonical\" href=\"https://boardgamegeek.com/thread/")) {
                        parentId = extractParentId(inputLine);
                        break;
                    }
                }
            }
        }

        return parentId;
    }

    private int extractParentId(String inputLine) {
        String workLine = inputLine.trim();
        String href = workLine.substring(workLine.indexOf("href=") + "href=\"https://boardgamegeek.com/thread".length(), workLine.lastIndexOf('"'));
        String stringId = href.substring(href.indexOf('/') + 1, href.lastIndexOf('/'));

        return Integer.parseInt(stringId);
    }
}
