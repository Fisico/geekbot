package org.fisico.geekbot.controller.actions;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;

import java.net.URI;

public abstract class BGGHttpPostAction extends BggHttpAction {

    protected HttpRequestBase getRequest(HttpEntity entity, URI bggUri) {
        final HttpPost request = new HttpPost(bggUri);
        request.addHeader(entity.getContentType());
        request.setEntity(entity);
        return request;
    }
}
