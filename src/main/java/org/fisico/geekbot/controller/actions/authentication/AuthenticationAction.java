package org.fisico.geekbot.controller.actions.authentication;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.fisico.geekbot.controller.actions.BGGHttpPostAction;
import org.fisico.geekbot.model.BGGSideError;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class AuthenticationAction extends BGGHttpPostAction {

    private final String PROTOCOL = "https";
    private final String HOST = "www.boardgamegeek.com";
    private final String ACTION = "/login";
    private final String INVALID_USERNAME_PASSWORD = "Invalid Username/Password";

    public LoginInfo authenticate(Credentials credentials) throws BGGAuthenticationException, BGGSideError {
        try {
            HttpClientContext context = HttpClientContext.create();
            HttpEntity entity = getHttpEntity(credentials);
            HttpResponse resp = execute(null, entity, context);
            return resolveResult(context, resp, credentials);
        } catch (BGGAuthenticationException e) {
            throw e;
        } catch (BGGSideError e) {
            throw e;
        } catch (Exception e) {
            throw new BGGAuthenticationException("Error during the authentication", e);
        }
    }

    protected HttpEntity getHttpEntity(Credentials credentials) throws BGGAuthenticationException {
        HttpEntity entity = null;

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("username", credentials.getUsername()));
        params.add(new BasicNameValuePair("password", credentials.getPassword()));
        try {
            entity = new UrlEncodedFormEntity(params);
        } catch (UnsupportedEncodingException e) {
            throw new BGGAuthenticationException("The credentials provided are not valid", e);
        }

        return entity;
    }

    @Override
    protected URI getUri() {
        URI bggUri = null;
        try {
            bggUri = new URIBuilder()
                    .setScheme(PROTOCOL)
                    .setHost(HOST)
                    .setPath(ACTION)
                    .build();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return bggUri;
    }

    private LoginInfo resolveResult(HttpClientContext context, HttpResponse resp, Credentials credentials) throws IOException, BGGAuthenticationException, BGGSideError {
        int code = resp.getStatusLine().getStatusCode();
        if (code == HttpStatus.SC_OK) {
            String responseText = readHTMLPage(resp.getEntity().getContent());
            if (!responseText.contains(INVALID_USERNAME_PASSWORD)) {
                CookieStore cookieStore = context.getCookieStore();
                return new LoginInfo(cookieStore);
            } else {
                throw new BGGAuthenticationException("The password for the user " + credentials.getUsername() + " is not correct", null);
            }
        } else {
            throw new BGGSideError("BGG is not available");
        }
    }
}
