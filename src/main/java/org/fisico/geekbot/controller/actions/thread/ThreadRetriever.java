package org.fisico.geekbot.controller.actions.thread;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.fisico.geekbot.controller.actions.BGGHttpGetAction;
import org.fisico.geekbot.model.GeekBotException;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

@Component
public class ThreadRetriever extends BGGHttpGetAction {

    private String PROTOCOL = "https";
    private String HOST = "www.boardgamegeek.com";
    private String API_URL = "xmlapi2";
    private String TYPE = "thread";
    private String PAR_ID = "id";
    private String PAR_LAST_ID = "minarticleid";

    private String currentAction;
    private List<NameValuePair> currentParams;

    public String retrieve(int id, int lastArticle) throws GeekBotException {
        try {
            currentAction = "/" + API_URL + "/" + TYPE;
            currentParams = new ArrayList<NameValuePair>();
            currentParams.add(new BasicNameValuePair(PAR_ID, String.valueOf(id)));
            currentParams.add(new BasicNameValuePair(PAR_LAST_ID, String.valueOf(lastArticle)));

            return useApi();
        } catch (Exception e) {
            throw new GeekBotException("Error retrieving posts from thread " + id, e);
        }
    }

    public String retrieveAll(int id) throws GeekBotException {
        try {
            currentAction = "/" + API_URL + "/" + TYPE;
            currentParams = new ArrayList<NameValuePair>();
            currentParams.add(new BasicNameValuePair(PAR_ID, String.valueOf(id)));

            return useApi();
        } catch (Exception e) {
            throw new GeekBotException("Error retrieving posts from thread " + id, e);
        }
    }

    @Override
    protected URI getUri() {
        URI bggUri = null;
        try {
            bggUri = new URIBuilder()
                    .setScheme(PROTOCOL)
                    .setHost(HOST)
                    .setPath(currentAction)
                    .addParameters(currentParams)
                    .build();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return bggUri;
    }

    protected String useApi() throws IOException {
        HttpClientContext context = HttpClientContext.create();
        HttpResponse resp = execute(null, null, context);
        return resolveResult(context, resp);
    }

    private String resolveResult(HttpClientContext context, HttpResponse resp) throws IOException {
        String result = "";
        int code = resp.getStatusLine().getStatusCode();
        if (code == HttpStatus.SC_OK) {
            result = readHTMLPage(resp.getEntity().getContent());
        }

        return result;
    }

}
