package org.fisico.geekbot.controller.actions.authentication;

import org.apache.http.client.CookieStore;
import org.apache.http.cookie.Cookie;

import java.time.Instant;
import java.util.Date;

public class LoginInfo {

    private CookieStore store;

    public LoginInfo(CookieStore cookieStore) {
        store = cookieStore;
    }

    public CookieStore getStore() {
        return store;
    }

    public boolean isValid() {
        Date now = Date.from(Instant.now());
        for (Cookie cookie : store.getCookies()) {
            if (cookie.isExpired(now)) {
                return false;
            }
        }

        return true;
    }
}
