package org.fisico.geekbot.controller.actions.mail;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.fisico.geekbot.controller.actions.BGGHttpPostAction;
import org.fisico.geekbot.controller.actions.authentication.LoginInfo;
import org.fisico.geekbot.model.GeekBotException;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

@Component
public class GeekMailer extends BGGHttpPostAction {
    private String PROTOCOL = "https";
    private String HOST = "www.boardgamegeek.com";
    private String ACTION = "/geekmail_controller.php";

    public boolean send(LoginInfo login, String to, String subject, String text) throws GeekBotException {
        try {
            HttpClientContext context = HttpClientContext.create();
            HttpEntity entity = getHttpEntity(to, subject, text);
            HttpResponse resp = execute(login, entity, context);
            return resolveResult(context, resp);
        } catch (Exception e) {
            throw new GeekBotException("Error sending geekmail to user " + to, e);
        }
    }

    @Override
    protected URI getUri() {
        URI bggUri = null;
        try {
            bggUri = new URIBuilder()
                    .setScheme(PROTOCOL)
                    .setHost(HOST)
                    .setPath(ACTION)
                    .build();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return bggUri;
    }

    protected HttpEntity getHttpEntity(String to, String subject, String text) {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("action", "save"));
        params.add(new BasicNameValuePair("messageid", ""));
        params.add(new BasicNameValuePair("touser", to));
        params.add(new BasicNameValuePair("subject", subject));
        params.add(new BasicNameValuePair("savecopy", "0"));
        params.add(new BasicNameValuePair("geek_link_select_1", ""));
        params.add(new BasicNameValuePair("sizesel", "10"));
        params.add(new BasicNameValuePair("body", text));
        params.add(new BasicNameValuePair("B1", "Send"));
        params.add(new BasicNameValuePair("folder", "inbox"));
        params.add(new BasicNameValuePair("label", ""));
        params.add(new BasicNameValuePair("ajax", "1"));
        params.add(new BasicNameValuePair("searchid", "0"));
        params.add(new BasicNameValuePair("pageID", "1"));

        final HttpEntity entity;
        try {
            entity = new UrlEncodedFormEntity(params);
        } catch (final UnsupportedEncodingException e) {
            // this should never happen.
            throw new IllegalStateException(e);
        }
        return entity;
    }

    private boolean resolveResult(HttpClientContext context, HttpResponse resp) throws IOException {
        boolean sended = false;
        int code = resp.getStatusLine().getStatusCode();
        if (code == HttpStatus.SC_OK) {
            String responseText = readHTMLPage(resp.getEntity().getContent());
            sended = recipientFound(responseText);
        }

        return sended;
    }

    private boolean recipientFound(String responseText) {
        boolean sended = false;
        if (!responseText.contains("not found")) {
            sended = true;
        }
        return sended;
    }
}
