package org.fisico.geekbot.controller.actions.thread;

import org.apache.http.*;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.fisico.geekbot.controller.actions.BGGHttpPostAction;
import org.fisico.geekbot.controller.actions.authentication.LoginInfo;
import org.fisico.geekbot.model.GeekBotException;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

@Component
public class ThreadCreator extends BGGHttpPostAction {

    private String PROTOCOL = "https";
    private String HOST = "www.boardgamegeek.com";
    private String ACTION = "/article/save";

    public int createThread(LoginInfo login, int id, String subject, String text) throws GeekBotException {
        try {
            HttpClientContext context = HttpClientContext.create();
            HttpEntity entity = getHttpEntity(id, subject, text);
            HttpResponse resp = execute(login, entity, context);
            return resolveResult(context, resp);
        } catch (Exception e) {
            throw new GeekBotException("Error creating thread in forum with id " + id, e);
        }
    }

    @Override
    protected URI getUri() {
        URI bggUri = null;
        try {
            bggUri = new URIBuilder()
                    .setScheme(PROTOCOL)
                    .setHost(HOST)
                    .setPath(ACTION)
                    .build();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return bggUri;
    }

    protected HttpEntity getHttpEntity(int id, String subject, String text) {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("action", "save"));
        params.add(new BasicNameValuePair("forumid", String.valueOf(id)));
        params.add(new BasicNameValuePair("articleid", ""));
        params.add(new BasicNameValuePair("objecttype", "region"));
        params.add(new BasicNameValuePair("objectid", "1"));
        params.add(new BasicNameValuePair("replytoid", ""));
        params.add(new BasicNameValuePair("geek_link_select_1", ""));
        params.add(new BasicNameValuePair("sizesel", "10"));
        params.add(new BasicNameValuePair("subject", subject));
        params.add(new BasicNameValuePair("body", text));

        final HttpEntity entity;
        try {
            entity = new UrlEncodedFormEntity(params);
        } catch (final UnsupportedEncodingException e) {
            // this should never happen.
            throw new IllegalStateException(e);
        }
        return entity;
    }

    private int resolveResult(HttpClientContext context, HttpResponse resp) {
        int createdId = -1;
        int code = resp.getStatusLine().getStatusCode();
        if (code == HttpStatus.SC_MOVED_TEMPORARILY) {
            Header locationHeader = resp.getFirstHeader("Location");
            String location = locationHeader.getValue();
            String stringId = extractId(location);
            createdId = Integer.parseInt(stringId);
        }

        return createdId;
    }

    private String extractId(String location) {
        return location.substring(location.lastIndexOf('/') + 1, location.indexOf('#'));
    }
}
