package org.fisico.geekbot.controller.actions;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;

import java.net.URI;

public abstract class BGGHttpGetAction extends BggHttpAction {

    protected HttpRequestBase getRequest(HttpEntity entity, URI bggUri) {
        return new HttpGet(bggUri);
    }
}
