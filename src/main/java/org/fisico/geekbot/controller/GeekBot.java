package org.fisico.geekbot.controller;

import org.fisico.geekbot.controller.actions.authentication.AuthenticationService;
import org.fisico.geekbot.controller.actions.authentication.BGGAuthenticationException;
import org.fisico.geekbot.controller.actions.authentication.Credentials;
import org.fisico.geekbot.controller.actions.authentication.LoginInfo;
import org.fisico.geekbot.controller.actions.mail.GeekMailer;
import org.fisico.geekbot.controller.actions.thread.PostCreator;
import org.fisico.geekbot.controller.actions.thread.ThreadCreator;
import org.fisico.geekbot.controller.actions.thread.ThreadFinder;
import org.fisico.geekbot.controller.actions.thread.ThreadRetriever;
import org.fisico.geekbot.model.BGGSideError;
import org.fisico.geekbot.model.BGGThread;
import org.fisico.geekbot.model.GeekBotException;
import org.fisico.geekbot.transform.XMLThreadScrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GeekBot implements IGeekBot {

    private AuthenticationService authenticationService;
    private GeekMailer geekMailer;
    private ThreadRetriever threadRetriever;
    private PostCreator postCreator;
    private ThreadCreator threadCreator;
    private ThreadFinder finder;
    private XMLThreadScrapper scrapper;

    @Autowired
    public GeekBot(AuthenticationService authenticationService, GeekMailer geekMailer, ThreadRetriever threadRetriever, PostCreator postCreator, ThreadCreator threadCreator, ThreadFinder finder, XMLThreadScrapper scrapper) {
        this.authenticationService = authenticationService;
        this.geekMailer = geekMailer;
        this.threadRetriever = threadRetriever;
        this.postCreator = postCreator;
        this.threadCreator = threadCreator;
        this.finder = finder;
        this.scrapper = scrapper;
    }

    @Override
    public boolean sendGeekMail(Credentials credentials, String toUser, String subject, String text) throws BGGAuthenticationException, BGGSideError, GeekBotException {
        LoginInfo login = authenticationService.authenticate(credentials);
        return geekMailer.send(login, toUser, subject, text);
    }

    @Override
    public BGGThread retrievePartialThread(int threadId, int lastArticle) throws GeekBotException, BGGSideError {
        String rawData = threadRetriever.retrieve(threadId, lastArticle);
        return scrapper.parse(rawData);
    }

    @Override
    public BGGThread retrieveCompleteThread(int threadId) throws BGGSideError, GeekBotException {
        String rawData = threadRetriever.retrieveAll(threadId);
        return scrapper.parse(rawData);
    }

    @Override
    public int replyPost(Credentials credentials, int replyTo, String subject, String text) throws BGGAuthenticationException, GeekBotException, BGGSideError {
        LoginInfo login = authenticationService.authenticate(credentials);
        return postCreator.reply(login, replyTo, subject, text);
    }

    @Override
    public int createThread(Credentials credentials, int forumId, String subject, String text) throws BGGAuthenticationException, GeekBotException, BGGSideError {
        LoginInfo login = authenticationService.authenticate(credentials);
        int postId = threadCreator.createThread(login, forumId, subject, text);
        return finder.findParentThreadId(postId);
    }
}