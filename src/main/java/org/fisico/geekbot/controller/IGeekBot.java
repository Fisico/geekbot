package org.fisico.geekbot.controller;

import org.fisico.geekbot.controller.actions.authentication.BGGAuthenticationException;
import org.fisico.geekbot.controller.actions.authentication.Credentials;
import org.fisico.geekbot.model.BGGSideError;
import org.fisico.geekbot.model.BGGThread;
import org.fisico.geekbot.model.GeekBotException;

public interface IGeekBot {
    boolean sendGeekMail(Credentials credentials, String toUser, String subject, String text) throws BGGAuthenticationException, BGGSideError, GeekBotException;

    BGGThread retrievePartialThread(int threadId, int lastArticle) throws BGGSideError, GeekBotException;

    BGGThread retrieveCompleteThread(int threadId) throws BGGSideError, GeekBotException;

    int replyPost(Credentials credentials, int replyTo, String subject, String text) throws BGGAuthenticationException, GeekBotException, BGGSideError;

    int createThread(Credentials credentials, int id, String subject, String text) throws BGGAuthenticationException, GeekBotException, BGGSideError;
}
