package org.fisico.geekbot.transform;

import org.springframework.stereotype.Component;

@Component
public class BGGFormatter {

    public String quote(String user, String text) {
        return "[q=\"" + user + "\"]" + text + "[/q]";
    }

    public String bold(String text) {
        return "[b]" + text + "[/b]";
    }

    public String thread(int id) {
        return "[thread=" + id + "][/thread]";
    }
}
