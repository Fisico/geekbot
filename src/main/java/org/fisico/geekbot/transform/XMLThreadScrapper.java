package org.fisico.geekbot.transform;

import org.fisico.geekbot.model.BGGPost;
import org.fisico.geekbot.model.BGGSideError;
import org.fisico.geekbot.model.BGGThread;
import org.fisico.geekbot.model.GeekBotException;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@Component
public class XMLThreadScrapper {

    public BGGThread parse(String response) throws BGGSideError, GeekBotException {
        BGGThread thread = null;
        DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder domBuilder = domFactory.newDocumentBuilder();
            try (BufferedInputStream is = new BufferedInputStream(new ByteArrayInputStream(response.getBytes(StandardCharsets.UTF_8)))) {
                Document document = domBuilder.parse(is);
                Element rootElement = document.getDocumentElement();
                String kind = rootElement.getTagName();
                if (!kind.equals("error")) {
                    thread = parseDocument(document);
                } else {
                    throw new BGGSideError(rootElement.getAttribute("message"));
                }
            }
        } catch (SAXException e) {
            throw new GeekBotException("Error parsing response from BGG", e);
        } catch (IOException e) {
            throw new GeekBotException("Error parsing response from BGG", e);
        } catch (ParserConfigurationException e) {
            throw new GeekBotException("Error parsing response from BGG", e);
        }

        return thread;
    }

    protected BGGThread parseDocument(Document document) {
        BGGThread thread;

        Element rootElement = document.getDocumentElement();

        int id = Integer.parseInt(rootElement.getAttribute("id"));
        Element subjectNode = (Element) rootElement.getElementsByTagName("subject").item(0);
        String subject = parseSimpleElement(subjectNode);
        thread = new BGGThread(id, subject);
        Element articles = (Element) rootElement.getElementsByTagName("articles").item(0);
        thread.setArticles(parseArticles(articles));

        return thread;
    }

    protected List<BGGPost> parseArticles(Element articles) {
        List<BGGPost> postList = new ArrayList<BGGPost>();
        NodeList nodeList = articles.getElementsByTagName("article");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                postList.add(parseArticle((Element) node));
            }
        }

        return postList;
    }

    protected BGGPost parseArticle(Element article) {
        BGGPost post;

        int id = Integer.parseInt(article.getAttribute("id"));
        String user = article.getAttribute("username");
        NodeList bodys = article.getElementsByTagName("body");
        NodeList subjects = article.getElementsByTagName(("subject"));
        Node bodyNode = bodys.item(0);
        String body = bodyNode.getTextContent();
        Node subjectNode = subjects.item(0);
        String subject = subjectNode.getTextContent();
        post = new BGGPost(id, user, subject, body);

        return post;
    }

    protected String parseSimpleElement(Element node) {
        return node.getTextContent();
    }
}
