### Geekbot ###

This project is a little library made to interact with the page http://www.boardgamegeek.com (also now as BGG) from java programs.

There are a few of other libraries that to this to some extent or other. This library has the goal to cover my own needs as I need it. The XML API of BGG keep changing and has very scarce documentation, so a full implementation of all the functionality is out of the scope. 

In his current state, the library could read threads of BGG, reply to posts and create new threads. Additionally, it could send geekmails. 

The main class is GeekBot. With this class you could use all the functionality of the library. The class Credentials is a holder for the credentials needed to identify yourself in BGG.

This work is licensed under LGPL and Apache License. 